import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from mod_tema import tema

#-------------------------
#Módulo de vista
#-------------------------

class Ventana:
    #La clase define un Marco contenedor
    def __init__(self, window=None):
        self.window = window
        self.window.title('Tarea POO')
        self.window.geometry('600x550')
        self.Frame = tk.Frame(self.window)
        self.Frame.grid(row=0, column=0)
        #Generador de Botones
        self.B = []
        self.L = []
        self.E = []
        self.Tree = []
        self.Datos = []
        self.Error = ''

        nomb = ['Mostrar registros existentes',
                   'Crear db', 'Alta']
        rowcolsp = [[3, 4, 4],[0, 0, 2],
                    [10, 4, 5],[2,50,150]]
        j=0

        for i in nomb:
            bot = tk.Button(self.Frame, text=i,
                            width=5)
            bot.grid(row=rowcolsp[0][j],
                     column=rowcolsp[1][j],
                     columnspan=rowcolsp[2][j],
                     sticky='w', padx=rowcolsp[3][j])
            self.B.append(bot)
            j+=1

        self.B[0].configure(width=71,
                            bd=2, background='grey65')

        nomb = ['Ingrese sus datos',
                'Título', 'Descripción']

        rowcolsp = [[0, 1, 2],[0, 0, 0],[10, 1, 1]]

        j=0

        for i in nomb:
            lab = tk.Label(self.Frame, text=i, width=10)
            lab.grid(row=rowcolsp[0][j], column=rowcolsp[1][j],
                     columnspan=rowcolsp[2][j], sticky='w')
            self.L.append(lab)
            j+= 1

        self.L[0].configure(width=75, background='violet',
                            foreground='yellow')

        rowcolsp=[[1,2],[2,2],[1,1]]

        for i in range(0,2):
            ent=tk.Entry(self.Frame, width=20)
            ent.grid(row=rowcolsp[0][i], column= rowcolsp[1][i],
                     columnspan=rowcolsp[2][i], padx=100)
            self.E.append(ent)

        self.__estilo = ttk.Style()
        self.__estilo.configure("custom.Treeview",
                                  highlightthickness=0,
                                  bd=0,
                                  font=('Calibri', 12)
                                  )
        self.__estilo.configure("custom.Treeview.Heading",
                                  font=('Calibri', 12),
                                  background='white'
                                  )
        self.__estilo.layout("custom.Treeview",
                               [('custom.Treeview.treearea',
                                 {'sticky': 'nswe'})]
                               )
        self.__tview = ttk.Treeview(self.Frame,
                                      height=15,
                                      columns=('#0','#1','#2'),
                                      style="custom.Treeview")
        self.__tview.heading('#0', text='ID')
        self.__tview.heading('#1', text='Título')
        self.__tview.heading('#2', text='Descripción')

        self.__tview.grid(row=5, column=0, columnspan=10,
                          sticky='w')

        self.Tree.append(self.__tview)

    def config_comando(self, indice, comando):
        self.B[indice].configure(command=comando)

    def impresion(self):
        try:
            for j in self.Tree[0].get_children():
                self.Tree[0].delete(j)
            for i in self.Datos:
                self.Tree[0].insert('','end',text=i[0],
                                    values=(i[1],i[2]))
        except:
            messagebox.showinfo('Atención','No hay elementos para mostrar')


    def get_entry(self,num):
        return self.E[num].get()


    def error(self):
        #Evalúa los errores y luego los imprime
        argum = str(self.Error)[0:4]
        if argum == '1007':
            ERROR='La base de datos ya existe'
        elif argum == '1048':
            ERROR="""Los datos ingresados contienen símbolos no permitidos"""
        elif argum == '1146':
            ERROR='La tabla "producto" no existe'
        elif argum == '1050':
            ERROR='La tabla "producto" ya existe'
        elif argum == '2003':
            ERROR='No se pudo conectar con la Base de Datos'
        elif argum == 'None':
            return
        else:
            #En caso que los códigos de error sean distintos:
            ERROR = self.Error

        messagebox.showinfo('Atención', ERROR)


class Sub_Ventana:
    def __init__(self, window, Marco = None):
        self.window = window
        self.Frame = tk.Frame(self.window)
        self.Frame.configure(background='red')
        self.Frame.grid(row = 1, column= 0, sticky='w')
        self.Radio = []
        self.Var = tk.IntVar()
        self.B = []
        self.color = 0
        self.Marco = Marco

        bot=tk.Button(self.Frame, text='Temas',
                      width=72, background='grey10',
                      foreground='red', command=self.valor)
        bot.grid(row=0, column=0, columnspan=10,
                 sticky='w')


        tema = ['Tema 1', 'Tema 2', 'Tema 3']
        rccs = [[1,5],[2,5],[3,5]]
        j=0
        for i in tema:

            rad = tk.Radiobutton(self.Frame, text=i,
                                 variable=self.Var,
                                 value=j, width=10,
                                 background='grey10',
                                 foreground='red')
            rad.grid(row=rccs[j][0], column=rccs[j][1],
                     sticky='w', columnspan=1)
            self.Radio.append(rad)
            j+=1

    def valor(self):
        self.color = self.Var.get()
        tema.cambio(self.color,self.Marco)


class Master_window:
    def __init__(self):
        self.master = tk.Tk()
        #Es utilizado try en caso que otros OS no reconozcan
        #el comando
        try:
            #Se deshabilita el comando maximizar
            #la vista está diseñada para un tamaño pre-establecido
            self.master.resizable(0,0)
        except:
            pass

        self.Contenedor1 = Ventana(self.master)
        self.Contenedor2 = Sub_Ventana(self.master, self.Contenedor1)


class Loop:
    def __init__(self):
        self.loop=tk.mainloop()



if __name__ == '__main__':
    Test=Master_window()
    Testloop=Loop()






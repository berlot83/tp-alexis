from os.path import dirname, abspath, join
import sys
import platform

#El código agrega la ruta del mod_vista, que contiene
#el módulo mod_tema -> el cual es importado por vista_U6
Dir_prog = dirname(__file__)
Dir_mod_tema = (join(Dir_prog, 'mod_vista'))
sys.path.append(Dir_mod_tema)
#Con esto se evita generar el paquete de instalación, y
#el programa funciona si se lo llama de cualquier ubicación

from mod_vista import vista_U6 as vista
from mod_modelo.c_mod_sql  import c_mod_sql as sql
from mod_modelo.c_mod_val import c_mod_val as valida

class Controlador:
    def __init__(self):
        self.model=sql.Admin(0,0)
        self.visual = vista.Master_window()
        self.modelc= sql.Generador()
        self.visual.Contenedor1.config_comando(0,self.__cons)
        self.visual.Contenedor1.config_comando(1,self.__crea)
        self.visual.Contenedor1.config_comando(2,self.__inserta)
    def __cons(self):

        self.visual.Contenedor1.Datos=self.model.consulta()
        self.visual.Contenedor1.impresion()

    def __crea(self):
        self.visual.Contenedor1.Error=self.modelc.tabla()
        self.visual.Contenedor1.error()

    def __inserta(self):
        valor1=self.visual.Contenedor1.get_entry(0)
        valor2=self.visual.Contenedor1.get_entry(1)
        self.visual.Contenedor1.Error=sql.Admin(valida.Validador(valor1).Validar(),
        valor2).insertar_tabla()
        self.visual.Contenedor1.error()




if __name__ == '__main__':
    cont=Controlador()
    vista.Loop()


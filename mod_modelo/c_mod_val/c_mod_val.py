import re

class Validador():
    def __init__(self,ing):
        self.__pat="^[A-Za-z0-9_-]+(?:[ _-][A-Za-z][0-9]+)*$"
        self.__ing=ing
    def Validar(self):
        patron=re.compile(self.__pat)
        res=patron.match(self.__ing)
        if res == None:
            return None
        else:
            return self.__ing


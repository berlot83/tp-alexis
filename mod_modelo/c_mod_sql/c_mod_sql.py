import mysql.connector
from mysql.connector import Error

class Generador:
    # Esta conexion debería ir aparte en un Objeto/Prototipo/Interface diferente, desde donde te marco
    connector = None

    # Patron => Singleton, se crea una sola instancia y si esta desconectado te lo vuelve a conectar, es la linea
    # comentada la otra cierra la conexion y reabre otra.
    # Si descomentas esta linea se activa el Singleton y no se crean mas insctancias, para probarlo corre Este archivo
    # sin tocar nada y mira los print, si la posición en memoria es la misma es que es la misma instancia, si cambia es
    # porque se cerró y se creó otra, eso pasaría donde dice is_conencted().
    @classmethod
    def connect_now(cls):
        #if cls.connector is None or cls.connector.is_connected() is not True:
        if cls.connector is None:
            try:
                cls.connector = mysql.connector.connect(
                                host="localhost",
                                user="root",
                                passwd="root",
                                database="baseprueba3",
                                auth_plugin='mysql_native_password')
            except mysql.connector.Error as error:
                return error
        return cls.connector

    # Usá este metodo cuando cierre una ventana o cuando sea logico cerrarla
    @classmethod
    def disconnect(cls):
        cls.connect_now().close()
    #End

    @classmethod
    def tabla(cls):
        cursor = cls.connect_now().cursor()
        cursor.execute("CREATE TABLE producto( id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, titulo VARCHAR(128) COLLATE utf8_spanish2_ci NOT NULL, descripcion text COLLATE utf8_spanish2_ci NOT NULL)")

class Admin:
        def __init__(self,Arg1,Arg2):
            self.__arg1=Arg1
            self.__arg2=Arg2

        def insertar_tabla(self):
                connection = Generador.connect_now()
                micursor= connection.cursor()
                sql="INSERT INTO producto (titulo,descripcion) VALUES (%s,%s)"
                datos=(self.__arg1, self.__arg2)
                micursor.execute(sql, datos)
                connection.commit()
                micursor.close()
                #Generador.disconnect() Esta agregala Solo si decidis descomentar la condicion is_connected en conexion
                print("Insert: " + str(connection.is_connected()))

        def consulta(self):
                connection = Generador.connect_now()
                micursor = connection.cursor()
                sql="SELECT * FROM producto"
                micursor.execute(sql)
                impresion = micursor.fetchall()
                print("Select: " + str(connection.is_connected()))
                micursor.close()
                #Generador.disconnect() Esta agregala Solo si decidis descomentar la condicion is_connected en conexion
                return impresion

if __name__ == '__main__':
    print(Generador.connect_now())
    print(Generador.connect_now())
    print(Generador.connect_now())
    print(Generador.connect_now())
    # Este metodo metelo en un boton o cuando cierra una ventana solo es una recomendacion, porque aca no te complica la
    # Te recomiendo que la llames donde esta el bon crear DB solo para probar, consulta y subi datos y uego toca el boton que llama a este metdo
    # cuando quieras volver a consultar o insertar datos la conexion deberia estar cerrada.

    # Si te decidis por el Singleton la conexion quedaabierta mientras dure la sesión de lo que hagas, y se cierra cuando vos lo quieras
    Generador.disconnect()
    print(Generador.connect_now())

    print('hello')
    print('hello')
